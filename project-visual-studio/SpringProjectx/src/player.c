/*
Copyright (C) 2015-2018 Parallel Realities

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.

See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.

*/

#include "common.h"

static SDL_Texture *pete[2];
static SDL_Texture* petew[2];

void initPlayer(void)
{
	player = malloc(sizeof(Entity));
	memset(player, 0, sizeof(Entity));
	stage.entityTail->next = player;
	stage.entityTail = player;

	player->health = 1;

	pete[0] = loadTexture("gfx/pete01.png");
	pete[1] = loadTexture("gfx/pete02.png");

	// wings
	petew[0] = loadTexture("gfx/petew01.png");
	petew[1] = loadTexture("gfx/petew02.png");

	player->texture = pete[0];
	SDL_QueryTexture(player->texture, NULL, NULL, &player->w, &player->h);
}


int left = 0;

void doPlayer(void)
{
	player->dx = 0;

	if (app.keyboard[SDL_SCANCODE_A])
	{
		player->dx = -PLAYER_MOVE_SPEED;
		left = 1;

		player->texture = pete[1];
	}

	if (app.keyboard[SDL_SCANCODE_D])
	{
		player->dx = PLAYER_MOVE_SPEED;
		left = 0;

		player->texture = pete[0];
	}

	if (app.keyboard[SDL_SCANCODE_W] && player->isOnGround)
	{
		player->riding = NULL;

		player->dy = -20;
		player->canDoubleJump = 1;

		playSound(SND_JUMP, CH_PLAYER);
	}
	else if (app.keyboard[SDL_SCANCODE_W] && !player->isOnGround && player->canDoubleJump && player->dy > 0) {
		player->riding = NULL;

		player->dy = -20;
		player->canDoubleJump = 0;

		playSound(SND_JUMP, CH_PLAYER);
	}

	if (app.keyboard[SDL_SCANCODE_SPACE])
	{
		player->x = player->y = 0;

		app.keyboard[SDL_SCANCODE_SPACE] = 0;
		stage.timeStarted = time(NULL);
	}

	if (player->powerUp) {
		player->dx *= 2;
		player->powerUp -= 1;
		stage.powerUp -= 1;
	}

	if (!player->isOnGround && player->dy < 0) {
		if (left) player->texture = petew[1];
		if (!left) player->texture = petew[0];
	}
	else {
		if (left) player->texture = pete[1];
		if (!left) player->texture = pete[0];
	}
}
